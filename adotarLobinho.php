<?php get_header(); ?>

    <main>
        <main>
            <div class="container">
                <div class = "conteudo">
                    <div class = "cabecalho">
                        <figure><img class = "redondo" src = "assets/lobo.png" width= "324" height= "216" ></figure>
                        <div class = "tituloESub">
                            <h1 class = "labelCampo" id = "nomeLobo">Adote o(a) NomeDoLobo</h1>
                            <p class = "labelCampo" id = "idLobo">ID:314159</p>
                        </div>
                    </div>
                    <div class= "divDoForm">
                        <form class= "forms">
                            <div class = "nomeIdade">
                                <div>
                                    <p class ="labelCampo">Seu nome:</p><br>
                                    <input type = "text" class = "inputFormsNome" placeholder=""><br>
                                </div>
                                <div>
                                    <p class = "labelCampo">Idade:</p><br>
                                    <input type = "number" class = "inputFormsIdade" placeholder=""><br>
                                </div>
                            </div>
                            <div>
                                <p class = "labelCampo" id = "emailAdota">E-mail:</p><br>
                                <input type=" text" class = "inputFormsMail" placeholder=""><br>
                            </div>
                            <div class ="divBotao"><button class ="azulBotao"><p>Adotar</p></button></div>

                        </form>

                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </main>
    </main>

<?php get_footer(); ?> 

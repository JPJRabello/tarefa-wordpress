<?php get_header(); ?>
    <main>
        <section class="bannerLobo centralizar">
            <h1>
                Adote um Lobinho
            </h1>
            <p class="loboSubtitulo centralizar">É claro que o consenso sobre a necessidade de qualificação apresenta tendências no sentido de aprovar a manutenção das regras de conduta normativas.</p>
        </section>
        <section class="caixa azul centralizar" id="caixaSobre">
            <h2>Sobre</h2>
            <p class="margemSobre">
                Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
            </p>
        </section>
        <section class="caixa cinza centralizar">
            <h2>Valores</h2>
            <div class="displayFlex">
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="<?php echo get_stylesheet_directory_uri() ?>./assets/life-insurance.svg" height="150" width="112"></figure>
                    <h3>Proteção</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="<?php echo get_stylesheet_directory_uri() ?>./assets/care.svg" height="160" width="112"></figure>
                    <h3>Carinho</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="<?php echo get_stylesheet_directory_uri() ?>./assets/care.svg" height="160" width="112"></figure>
                    <h3>Companheirismo</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="<?php echo get_stylesheet_directory_uri() ?>./assets/care.svg" height="160" width="112"></figure>
                    <h3>Resgate</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
            </div>
        </section>
        <section class="caixaVazia">
            <h2 class="centralizar">Lobos Exemplo</h2>
            <div class="mostraLobo1 displayFlex">
                <figure><img class="loboSombraReverso" src='<?php echo get_stylesheet_directory_uri() ?>./assets/lobo.png' width=450 height=327.75></figure>
                <div>
                    <h3>Lobo Cinza</h3>
                    <sub>Idade: 08 anos</sub>
                    <br>
                    <p class="caption">
                    Delectus quia eaque. Architecto ipsa ducimus. Temporibus fugit mollitia.
                    Provident odio aliquid. Praesentium quia quia. Deserunt vel beatae. 
                    Aut adipisci itaque. Consequatur ut consequuntur. Corporis earum magnam. 
                    Nesciunt asperiores repellendus. Est lab.
                    </p>
                </div>
            </div>
            <div class="mostraLobo2 displayFlex">
                <figure><img class="loboSombraReverso" src='<?php echo get_stylesheet_directory_uri() ?>./assets/lobo-branco.png' width=450 height=327.75></figure>
                <div>
                    <h3>Lobo Branco</h3>
                    <sub>Idade: 13 anos</sub>
                    <br>
                    <p class="caption">
                    Consequatur sed et. Ut vitae officia. Explicabo distinctio veritatis. 
                    Voluptas rem aliquam. Placeat sed quia. Repellat consequatur accusamus.
                    Dolor distinctio qui. Sit rerum non. Amet aliquid et. Delectus qui incidunt. 
                    Magni et et. Sit laboriosam aperiam.
                    </p>
                </div>
            </div>
        </section>
        
    </main>
    
<?php get_footer(); ?> 

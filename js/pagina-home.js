const $ = document
const list = $.querySelector('.caixaVazia')
const url = 'https://lobinhos.herokuapp.com/wolves/'
// const path = '/wolves/'

const inserirNoSite = (nome, idade, desc, img, id,side) =>{


  // 
  let novo_lobo = $.createElement("div")
  novo_lobo.id = "div-group" + id

  var lado = 0;
  for (var i = 0; i<3;i++){
    if (lado==0) {
      // lado+=1;
  
      novo_lobo.classList.add("loboFoto")
      novo_lobo.innerHTML = `
      <figure><img class="loboSombra" src=${img} width=450 height=327.75></figure>
      <div>
          <h3>${nome}</h3>
          <sub>Idade: ${idade} anos</sub>
          <br>
          <p class="caption">
              ${desc}
          </p>
      </div>
      `
    } else {
  
  
      novo_lobo.classList.add("loboFoto","reverso")
      novo_lobo.innerHTML = `
      <figure><img class="loboSombraReverso" src=${img} width=450 height=327.75></figure>
      <div>
          <h3>${nome}</h3>
          <sub>Idade: ${idade} anos</sub>
          <br>
          <p class="caption">
              ${desc}
          </p>
      </div>
      `
  }
  lado+=1;

  }
  console.log(list,novo_lobo);
  list.appendChild(novo_lobo)
}


//GET
fetch(url)
  .then(resp => resp.json())
  .then(element => {
    
    inserirNoSite(element.wolves[0].name,element.wolves[0].age,element.wolves[0].description,element.wolves[0].link_image,0,true);
    inserirNoSite(element.wolves[1].name,element.wolves[1].age,element.wolves[1].description,element.wolves[1].link_image,1,false)
  })
  .catch(error => console.warn(error))


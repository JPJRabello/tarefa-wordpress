const $ = document
// const list = $.querySelector('.caixaVazia')
// document.addEventListener("DOMContentLoaded", function(){
//   const list = $.querySelectorAll('.caixaVazia');
//   console.log(itens);
// });
const url = 'https://lobinhos.herokuapp.com'
const path = '/wolves/'

const inserirNoSite = (nome, idade, desc, img, id,side) =>{
  let novo_lobo = $.createElement("div")
  novo_lobo.id = "div-group" + id
  if (side) {
    novo_lobo.classList.add("loboFoto")
    novo_lobo.innerHTML = `
    <figure><img class="loboSombra" src=${img} width=450 height=327.75></figure>
    <div>
        <h3>${nome}</h3>
        <sub>Idade: ${idade} anos</sub>
        <br>
        <p class="caption">
            ${desc}
        </p>
    </div>
    `
  } else {
    novo_lobo.classList.add("loboFoto","reverso")
    novo_lobo.innerHTML = `
    <figure><img class="loboSombraReverso" src=${img} width=450 height=327.75></figure>
    <div>
        <h3>${nome}</h3>
        <sub>Idade: ${idade} anos</sub>
        <br>
        <p class="caption">
            ${desc}
        </p>
    </div>
    `
  }
  list.appendChild(novo_lobo)
}

//GET
fetch(url+path)
  .then(resp => resp.json())
  .then(element => {
    for (let i = 0; i < element.wolves.length; i++) {
      if (i%2 == 0) {
        inserirNoSite(element.wolves[i].name,element.wolves[i].age,element.wolves[i].description,element.wolves[i].link_image,i,true)
      } else {
        inserirNoSite(element.wolves[i].name,element.wolves[i].age,element.wolves[i].description,element.wolves[i].link_image,i,false)
      }
    }
  })
  .catch(error => console.warn(error))

/* // Chamei as funções da API
import { url, path, getWolves, getWolvesAdopted } from './api.js'
const lobos = getWolves()

// Esse for vai gerar as divs com os Lobos
for (i = 0; i < LOBO.length; i++) {
  let caixaVazia = document.createElement('div')
  caixaVazia.classList.add('caixaVazia')

  let loboFoto = document.createElement('div')
  loboFoto.classList.add('loboFoto')
  let div = document.createElement('div')

  // Ponto a foto
  let figure = document.createElement('figure')
  let foto_lobo = document.createElement('img')
  foto_lobo.classList.add('loboSombra')
  foto_lobo.id = 'lobo' + i + 1 + 'Exemplo'
  foto_lobo.src = LOBO[i].link_image
  foto_lobo.width = '450'
  foto_lobo.height = '327.75'
  figure.appendChild(foto_lobo)

  loboFoto.appendChild(foto_lobo)
  loboFoto.appendChild(figure)
  loboFoto.appendChild(div)

  // Pondo o nome
  let nome_lobo = document.createElement('h3')
  nome_lobo.id = 'nome-do-lobo' + i + 1
  nome_lobo.innerText = LOBO[i].name
  // Criando botão "Adotar"
  let adotar = document.createElement('button')
  adotar.innerText = 'adotar'
  // Pondo a idade
  let idade_lobo = document.createElement('sub')
  idade_lobo.id = 'idade' + i + 1
  idade_lobo.innerText = `Idade: ${LOBO[i].age} anos`
  let br = document.createElement('br')
  // Pondo a descrição
  let texto_lobo = document.createElement('p')
  texto_lobo.id = '#texto-lobo' + i + 1
  texto_lobo.innerHTML = LOBO[i].description

  div.classList.add('div-sem-nome')
  div.appendChild(nome_lobo)
  div.appendChild(adotar)
  div.appendChild(idade_lobo)
  div.appendChild(br)
  div.appendChild(texto_lobo)

  caixaVazia.appendChild(loboFoto)

  let main = document.querySelector('main')
  main.appendChild(caixaVazia)
}

// Gerar as divs com os lobos adotados caso o checkbox esteja marcado
const lobosAdotados = getWolvesAdopted()
let checkbox = document.querySelector('.checkbox')
let divAdotados = document.createElement('div')

for (i = 0; i == lobosAdotados.length - 1; i++) {
  let caixaVazia = document.querySelector('.caixavazia')

  let loboFoto = document.createElement('div')
  loboFoto.classList.add('loboFoto')

  // Ponto a foto
  let figure = document.createElement('figure')
  let foto_lobo = document.createElement('img')
  foto_lobo.classList.add('loboSombra')
  foto_lobo.id = 'lobo' + i + 1 + 'Exemplo'
  foto_lobo.src = lobosAdotados[i].link_image
  foto_lobo.width = '450'
  foto_lobo.height = '327.75'
  figure.appendChild(foto_lobo)

  loboFoto.appendChild(foto_lobo)
  loboFoto.appendChild(figure)
  loboFoto.appendChild(div)

  // Pondo o nome
  let nome_lobo = document.createElement('h3')
  nome_lobo.id = 'nome-do-lobo' + i + 1
  nome_lobo.innerText = lobosAdotados[i].name
  // Criando botão "Adotar"
  let adotar = document.createElement('button')
  adotar.innerText = 'adotar'
  // Pondo a idade
  let idade_lobo = document.createElement('sub')
  idade_lobo.id = 'idade' + i + 1
  idade_lobo.innerText = `Idade: ${LOBO[i].age} anos`
  let br = document.createElement('br')
  // Pondo a descrição
  let texto_lobo = document.createElement('p')
  texto_lobo.id = '#texto-lobo' + i + 1
  texto_lobo.innerHTML = lobos[i].description

  let div = document.createElement('div')
  div.classList.add('div-sem-nome')
  div.appendChild(nome_lobo)
  div.appendChild(adotar)
  div.appendChild(idade_lobo)
  div.appendChild(br)
  div.appendChild(texto_lobo)

  caixaVazia.appendChild(loboFoto)
  divAdotados.appendChild(caixaVazia)
}
checkbox.addEventListener('change', event => {
  divAdotados.hidden = event.currentTarget.checked
})
 */
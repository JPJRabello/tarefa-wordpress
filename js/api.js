const url = 'lobinhos.herokuapp.com/'
const path = 'wolves/'

//retorna um Array de todos os lobos disponíveis para adoção
//{id, name, age, link_image, description}
function getWolves() {
  fetch(url + path)
    .then(resp => resp.json())
    .catch(error => console.warn(error))
  return resp
}

//retorna Info do Lobo específico, com informações adicionais se ele foi adotado
//adoption: {id, wolf_id, name, age, email, created_at, updated_at}
function getWolvesByID(id) {
  fetch(url + path + id)
    .then(resp => resp.json())
    .catch(error => console.warn(error))
}

//retorna um Array de todos os lobos já adotados
//{id, name, age, link_image, description}
function getWolvesAdopted() {
  fetch(url + path + 'adopted')
    .then(resp => resp.json())
    .catch(error => console.warn(error))
}

//deleta um Lobo da API
function deleteWolves(id) {
  const fetchConfig = { method: 'DELETE' }
  fetch(url + path + id, fetchConfig).catch(error => console.warn(error))
}

//cria um novo Lobo
function createWolves(nome, age, img, desc) {
  if (validateWolves(nome, age, img, desc) == true) {
    let fetchBody = {
      wolf: {
        name: nome,
        age: age,
        link_image: img,
        description: desc
      }
    }
    let fetchConfig = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(fetchBody)
    }
    fetch(url + path, fetchConfig)
      .then(resp => resp.json())
      .catch(error => console.warn(error))
  } else {
    console.log('Dados não passaram pela validação')
  }
}

function adoptWolves(nome, age, email, wolf_id) {
  if (validateAdoption(nome, age, email) == true) {
    let fetchBody = {
      adoption: {
        name: nome,
        age: age,
        email: email,
        wolf_id: wolf_id
      }
    }
    let fetchConfig = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(fetchBody)
    }
    fetch(url + path + 'adoption', fetchConfig)
      .then(resp => resp.json())
      .catch(error => console.warn(error))
  } else {
    console.log('Dados não passaram pela validação')
  }
}

//Funções para validar informações

function validateWolves(nome, age, img, desc) {
  return (
    inRange(nome.length, 4, 60) &&
    inRange(age, 0, 100) &&
    img != '' &&
    inRange(desc.length, 10, 255)
  )
}

function validateAdoption(nome, age, email) {
  if (inRange(nome.length, 4, 60) && inRange(age, 0, 60)) {
    //conferir de e-mail é único
    let valid
    fetch(url + path + 'adopted')
      .then(resp => resp.json())
      .then(lobo => (valid = lobo.every(element => element.email != email)))
    return valid
  } else {
    return false
  }
}

//Função para checar se valor está entre certo range
const inRange = (x, min, max) => (x - min) * (x - max) <= 0

export {
  url,
  path,
  getWolves,
  getWolvesByID,
  getWolvesAdopted,
  deleteWolves,
  createWolves,
  adoptWolves,
  validateWolves,
  validateAdoption,
  inRange
}
